Sub Check_For_Ticket(MyMail As MailItem)
    On Error GoTo Proc_Error

    Dim strTicket, strSubject As String
   
    ' Default value in case # is not found in the subject line
    strTicket = "None"
   
    ' Grab the subject from the message
    strSubject = MyMail.Subject
   
    ' See if it has a hash symbol in it
    If InStr(1, strSubject, "#") > 0 Then
   
        ' Trim off leading stuff up to and including the hash symbol
        strSubject = Mid(strSubject, InStr(strSubject, "#") + 1)
       
        ' Now find the trailing space after the ticket number and chop it off after that
        If InStr(strSubject, " ") > 0 Then
            strTicket = Left(strSubject, InStr(strSubject, " ") - 1)
        End If
    End If
    MsgBox "Your Ticket # is: " & strTicket
   
Proc_Done:
    Exit Sub
   
Proc_Error:
    MsgBox "An error has occured in Check_For_Ticket. Error #" & Err & " - " & Err.Description
    GoTo Proc_Done
End Sub

 