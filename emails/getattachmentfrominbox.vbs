Sub GetAttachmentFromInbox()
 Dim ns As Namespace
 Dim Inbox As MAPIFolder
 Dim Item As Object
 Dim OlItems As Object
 Dim olMail As Object
 Dim Atmt As Attachment
 Dim FileName As String
 Dim strPrefix As String
 Dim strSearch As String
 Dim strSubject As String
 Dim ExtString As String
 Dim ExtString2 As String
 Dim strSavePath As String
 Dim strAttachmentName As String
 Dim strMyEmail As String
 Dim strSearchFolder As String


 ' User Variables

 strSearch = "Stock On-hand Cloud Suite"
 ExtString = "xlsx" ' must be 4 characters long
 ExtString2 = ".xls" ' must be 4 characters long
 strSavePath = "C:\Users\NJorgense\Patties Foods\Group Operations - Inventory\Variance Reports\"
 strMyEmail = "natasha.jorgensen@patties.com.au"
 strSearchFolder = "SOH Cloud Suite vs Paperless" ' must be at root level
 
 On Error Resume Next

 Set OlApp = GetObject(, "Outlook.Application")
 Set olns = OlApp.GetNamespace("MAPI")
 
 If Err.Number = 429 Then
     Set OlApp = CreateObject("Outlook.application")
 End If

 Set OlFldr = olns.Folders(strMyEmail) 'name of the mailbox as found in Outlook, via aco**** settings
        
 Set OlFldr = OlFldr.Folders.Item(strSearchFolder) 'set folder to look for email


If OlFldr Is Nothing Then 'Check if OlFldr is found
 MsgBox "Folder not found"
 GoTo Ready
End If

Set OlItems = OlFldr.Items 'Declare mails as items
OlItems.Sort "[ReceivedTime]", True 'Sort mail from new to old

For Each olMail In OlItems     'Loop through all emails
        MsgBox olMail.Subject ' going through all of inbox
    If InStr(olMail.Subject, strSearch) <> 0 And olMail.UnRead = True Then
        If olMail.Attachments.Count > 0 Then 'check for attachments
            For i = 1 To olMail.Attachments.Count 'loop through all attachments
                strAttachmentName = olMail.Attachments.Item(i).FileName
                If (Right(strAttachmentName, 4) = ExtString) Or (Right(strAttachmentName, 4) = ExtString2) Then
                    ' This path must exist! Change folder name as necessary.
                        FileName = strSavePath & Format(olMail.CreationTime, "yyyymmdd_") & strAttachmentName
                        olMail.Attachments.Item(i).SaveAsFile FileName
                        GoTo Finalise
                End If
            Next
        End If
    End If
Next
Finalise:
' finalise

Ready:
'clean up
''Set olFolder = Nothing
'Set OlItems = Nothing
'Set olMail = Nothing
'Set OlApp = Nothing

End Sub

