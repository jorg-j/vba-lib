' Declare Variables
Dim RowPosition as Integer
Dim ColPosition as Integer
Dim InSheet as String
Dim OutSheet as String
Dim conn As Object, rst As Object
Set conn = CreateObject("ADODB.Connection")
Set rst = CreateObject("ADODB.Recordset")

RowPosition = 1
ColPosition = 1
InSheet = ""
OutSheet = ""

' Open Connection
conn.Open "DRIVER=SQLite3 ODBC Driver;Database=C:\MyDB.db;"

'Create DB
strSQL = "
CREATE TABLE IF NOT EXISTS 'matdat' (
    'id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    'date' NUMERIC NOT NULL DEFAULT (date('now')),
    'material' TEXT NOT NULL,
    'qty' INTEGER NOT NULL
);
"

rst.Open strSQL, conn
rst.Close


For RowPosition To 6

    keyCell = Worksheets(InSheet).Cells(RowPosition, ColPosition).Value
    countCell = Worksheets(InSheet).Cells(RowPosition, ColPosition+1).Value

    ' Add in Items

    strSQLLoadItem = "INSERT INTO 'matdat'('material','qty') VALUES (" & keyCell & "," & countCell & ");"

    rst.Open strSQLLoadItem, conn
    rst.Close

Next RowPosition

 
'Query n Days
strSQLQuery = "
SELECT material, SUM(qty), Count(*) Occurance
FROM matdat
WHERE date BETWEEN date('now', '-5 day') AND date('now')
GROUP BY material;
"

rst.Open strSQLQuery, conn

Worksheets(OutSheet).Range("A1").CopyFromRecordset rst
rst.Close

Set rst = Nothing: Set conn = Nothing